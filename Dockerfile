# Please check https://gitlab.com/snippets/1972896 for more details
# Idea taken of https://invidio.us/watch?v=D2E1Eh9Hxdg
# Usage: docker build -t carlosal1015/texlive:v1.0 .

ARG VERSION=latest
FROM registry.gitlab.com/islandoftex/images/texlive:$VERSION as base

MAINTAINER Oromion <caznaranl@uni.pe>

# basic stuff
RUN echo 'APT::Get::Assume-Yes "true";' >> /etc/apt/apt.conf \
    && apt update && apt dist-upgrade \
# Emacs
    && apt install emacs-nox \
    elpa-pdf-tools \
    git \
# Cleanup
    && apt autoremove \
    && rm -rf /tmp/* /var/lib/apt/lists/* /root/.cache/*

FROM base

RUN wget https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.zip \
    && unzip 1.050R-it.zip \
    && mkdir -p ~/.fonts \
    && cp source-code-pro-*-it/OTF/*.otf ~/.fonts/ \
    && fc-cache -f -v

RUN git clone --depth=1 https://github.com/syl20bnr/spacemacs ~/.emacs.d

#ENTRYPOINT ["emacs"]
